export enum STATUS {
    READY = "rgb(64, 111, 0)",
    ACTIVE = "rgb(64, 152, 0)",
    WAITING = "rgb(170, 150, 10)",
    INACTIVE = "rgb(110, 110, 110)",
    EMERGENCY = "rgb(237, 34, 0)"
}